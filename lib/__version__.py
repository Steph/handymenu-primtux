"""
HandyMenu :     menu principal de la distribution
                HandyLinux <http://handylinux.org>

Auteurs :
            - HandyMenu-v1/2 : aka handy-menu
                arnault perret <arpinux@member.fsf.org>
                Meier Link <lucian.von.ruthven@gmail.com>
                manon crunchiikette <contact@handylinux.org>
                etienne de paris <contact@handylinux.org>
                fibi bestesterever <contact@handylinux.org>

            - HandyMenu v3 : aka handymenu
                Xavier Cartron (thuban@yeuxdelibad.net)
                Modifié par Tomasi pour PrimTux

            - HandyMenu v4 : aka handymenu
                Modifié par Jérémie Ferry alias Mothsart (ferryjeremie@free.fr)

licence :       GNU General Public Licence v3
Description :   Handymenu from scratch
Dépendances :   python-gtk2

"""

version = "1.2.6"
auteur = "thuban modifié par Mothsart"
licence = "GPLv3"
homepage = "http://handylinux.org, http://primtux.fr"
