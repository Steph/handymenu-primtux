from gi.repository import Gtk

from .dialog.icon_chooser import IconChooserDialog
from .hm_utils import new_img


class SectionConfig():

    def delete(self, section, notebook):
        index_page = notebook.get_current_page()
        notebook.remove_page(index_page)
        if index_page - 1 > 0:
            notebook.set_current_page(index_page - 1)
        self.config.remove(section)
        self.utils.save_config(self.config)

    def set_icon(self, section, section_menu, label, notebook):
        chooser = IconChooserDialog(self.utils)
        response = chooser.run()

        if response == Gtk.ResponseType.CANCEL:
            print(self.utils._('Closed, no files selected'))
        elif response == Gtk.ResponseType.OK:
            icon_path = chooser.get_filename()

            # Create new tab content
            hbox = Gtk.HBox()
            img = new_img(icon_path)
            if img:
                hbox.pack_start(img, False, True, 0)

            hbox.pack_end(Gtk.Label(label), True, True, 0)
            hbox.show_all()

            # Replace tab by the new content
            current_index = notebook.get_current_page()
            page = notebook.get_nth_page(current_index)
            notebook.set_tab_label(page, hbox)

            # Update yaml
            self.utils.mod_section_icon(self.config, section, icon_path)
        # close chooser dialog
        chooser.destroy()

    def __init__(self, utils, config):
        self.utils = utils
        self.config = config

