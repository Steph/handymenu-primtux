from gi.repository import Gtk, Gdk

def gtk_style():
    css = b"""
    .onhover {
        border-color: Grey;
    }

    grid {
        padding: 20px;
    }
    """
    style_provider = Gtk.CssProvider()
    style_provider.load_from_data(css)

    Gtk.StyleContext.add_provider_for_screen(
        Gdk.Screen.get_default(),
        style_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    )
