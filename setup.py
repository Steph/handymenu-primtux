#!/usr/bin/env python3

import os
import glob
from setuptools import setup
from lib import __version__

I18NFILES = []
for filepath in glob.glob("locale/*/LC_MESSAGES/*.mo"):
    lang = filepath[len("locale/"):]
    targetpath = os.path.dirname(os.path.join("share/locale", lang))
    I18NFILES.append((targetpath, [filepath]))

setup(
    name='handymenu',
    version=__version__.version,
    description=(
        '...'
    ),
    author='mothsart',
    author_email='ferryjeremie@free.fr',
    url='https://framagit.org/Steph/handymenu-primtux',
    packages=[ 'lib' ],
    data_files= I18NFILES
)
