# handymenu-primtux 

(modifiés par mothsart)

## Utilisation pour des utilisateurs personnalisés

Primtux utilise 4 sessions par défaut : session mini, super, maxi et prof
Il est possible de rajouter des utilisateurs.

Pour attribuer à un utilisateur un handymenu, il vous suffit d'appeler de vous connecter avec cet utilisateur et appeler le script suivant :

```bash
  handymenu-custom
```

Le handymenu cherchera et éditera sa configuration dans /home/{utilisateur}/.config/handymenu/

4 fichiers sont indentifiables à cet endroit :

- conf.default.yaml
- conf.yaml
- menu.png
- noclose.conf (facultatif)

Même principe pour la configuration du handymenu

```bash
  handymenu-configuration-custom
```

## Utilisation pour des handymenus thématiques

Il est possible d'avoir d'autres handymenus sur la même session.
Pour celà, il faut se créer un répertoire contenant 2 à 4 fichiers :

- conf.default.yaml
- conf.yaml (facultatif)
- menu.png
- noclose.conf (facultatif)

Pour lancer ce handymenu, il suffit simplement d'appeler ce script avec comme argument le chemin relatif ou absolu de ce répertoire :

```bash
  handymenu-custom chemin
```
Vous disposez d'un exemple :

```bash
  handymenu-custom conf_test
```

Même principe pour la configuration du handymenu

```bash
  handymenu-configuration-custom chemin
```

## Différentiel

Il est possible de rajouter ou supprimer une liste de logiciels via un petit utilitaire.

Pour l'ajout :

```bash
  menudiff <utilisateur> add <file.yaml>
```

Pour la suppression :

```bash
  menudiff <utilisateur> remove <file.yaml>
```

## Mode Debug

Rajouter le paramètre **-v** ou **--verbose** : cette action renvoi la version du handymenu(à partir de la 1.2.5) ainsi que le fichier de conf et le contenu du fichier de conf.

Exemples :

```bash
  handymenu-mini -v
  handymenu-custom conf_test --version
```

## Creation d'un paquet Debian

```sh
git clone https://framagit.org/Steph/handymenu-primtux.git handymenu
cd handymenu
debuild -us -uc
```
